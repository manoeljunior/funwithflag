import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './../node_modules/animate.css'
import './../node_modules/@fortawesome/fontawesome-free/css/all.min.css'
import Header from '@/components/Header.vue'

Vue.component('header-game', Header)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
