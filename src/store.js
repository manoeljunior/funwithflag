import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    nickname: '',
    mode: 12,
    newGame: false,
    moves: 0,
    gameTime: 0,
    canPlay: false,
    countries: [
      'ARG', 'AUT', 'BEL', 'BRA', 'CAN', 'CHI',
      'CHN', 'CMR', 'COL', 'CRO', 'DEN', 'EGY',
      'ESP', 'FRA', 'GBR', 'GER', 'ISL', 'ITA',
      'JPN', 'KOR', 'KSA', 'MAR', 'MEX', 'NGA',
      'NZL', 'PAN', 'POR', 'PUR', 'ROU', 'RUS',
      'SWE', 'TUN', 'URU', 'USA', 'WAL', 'YUG'
    ],
    board: [ ],
  },
  getters: {
    isNewGame: state => {
      return state.newGame
    },
    mode: state => {
      return state.mode
    },
    nickname: state => {  
      return state.nickname 
    },
    getCountries: state => {
      return state.countries
    },
    getBoard: state => {
      return state.board
    },
    moves: state => {
      return state.moves
    },
    gameTime: state => {
      return state.gameTime
    },
    canPlay: state => {
      return state.canPlay
    },
    isFinished: state => {
      return !state.board.some(flag => {
        return flag.active !== false
      })
    }
  },
  mutations: {
    toggleNewGame (state, payload) {
      state.newGame = payload
    },
    setUser (state, payload) {
      if (payload) {
        state.nickname = payload
      } else {
        state.nickname = 'Anonymous'
      }
    },
    setMode (state, payload) {
      state.mode = payload
    },
    // createNewGame (state, payload) {
    //   state.newGame = true
    //   state.nickname = payload.nickname
    // },
    generateBoard (state, payload) {
      state.board = payload
    },
    increaseMoves (state) {
      state.moves++
    },
    resetMoves (state) {
      state.moves = 0
    },
    toggleCanPlay (state, payload) {
      state.canPlay = payload
    },
    setGameTime (state, payload) {
      state.gameTime = payload
    }
  },
  actions: {
    prepareGame({ commit, state }) {
      commit('toggleNewGame', true)
      commit('setMode', state.mode)
      commit('resetMoves')
    }
  }
})
