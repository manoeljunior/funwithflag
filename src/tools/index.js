const hasBestPlayer = (mode) => {
    let bestPlayer = localStorage.getItem('bestPlayer')
    if (bestPlayer) {
        let record = JSON.parse(bestPlayer)[mode]
        return record ? true : false
    }
    return false
}

const bestPlayer = (mode) => {
    if (hasBestPlayer(mode)) {
        let bestPlayer = JSON.parse(localStorage.getItem('bestPlayer')) 
        return bestPlayer[mode]
    }
    return null
}

const setBestPlayer = (bestPlayer) => {
    localStorage.setItem('bestPlayer', JSON.stringify(bestPlayer))
}

module.exports = { hasBestPlayer, bestPlayer, setBestPlayer }
